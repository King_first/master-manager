<?php

use Xianlin\MasterManager\Models\Master as MasterModel;

class MasterTest extends \Tests\TestCase
{
    public function test_set_master()
    {
        $set_result = master([
            'name' => '测试设置master数据名称',
            'en_name' =>'test_set_master',
            'explain' =>'测试设置master数据说明',
            'data'=>json_encode([
                'key1'=>'val1',
                'key2'=>'val2',
                'key3'=>'val3',
            ]),
        ]);

        //$this->assertInstanceOf(MasterModel::class, $set_result);
        $this->assertTrue($set_result);
    }

    public function test_get_master()
    {
        $get_result = master('test_set_master');

        $this->assertIsArray($get_result);
    }
}
