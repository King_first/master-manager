# Master Data Manager

Simple package to manager master data .

## Installation

> **Requires:**
- **[PHP 7.2.5+](https://php.net/releases/)**
- **[Laravel 6.0+](https://github.com/laravel/laravel)**

You can install the package via composer:

```sh
composer require xianlin/master-manager
```

This package supports Laravel [Package Discovery][link-package-discovery].

### Publish configuration file

`php artisan vendor:publish --provider="Xianlin\MasterManager\MasterManagerServiceProvider"`


