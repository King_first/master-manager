<?php

namespace Xianlin\MasterManager\Models;


use Illuminate\Database\Eloquent\Model;

class Master extends Model
{

    protected $table = 'masters';


    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name','en_name','explain','data'
    ];

    /**
     * @param $data
     * @return array|mixed
     */
    public function getDataAttribute($data)
    {
        return json_decode($data, true) ?: [];
    }
}
