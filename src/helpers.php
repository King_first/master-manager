<?php
use \Xianlin\MasterManager\Libraries\Master;

if(!function_exists('master')) {
    /**
     * 设置或者获取master数据
     * @param string|array $en_name
     * @param null $key
     * @param null $default
     * @return mixed
     */
    function master($en_name, $key = null, $default = null)
    {
        if(is_string($en_name) && is_null($key)){
            return Master::list($en_name);
        }
        if (is_array($en_name)){
            return Master::set($en_name);
        }

        return Master::get($en_name, $key, $default);
    }
}
